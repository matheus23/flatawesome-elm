# FlatAwesome

A simple clone of [Flattastic](https://flatastic-app.com/). Built with [Elm](https://elm-lang.org) and [Kinto](https://kinto-storage.org).

A pet project as of now.
