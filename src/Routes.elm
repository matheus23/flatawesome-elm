module Routes exposing
    ( FlatRoute(..)
    , Info
    , Route(..)
    , filterExternal
    , parse
    , pushUrl
    , replaceUrl
    , toHref
    )

import Browser
import Browser.Navigation as Navigation
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, s)


type alias Info =
    { navigationKey : Navigation.Key
    , publicPath : List String
    }


type Route
    = Top
    | Login
    | ChooseFlat
    | Flat String FlatRoute


type FlatRoute
    = Overview
    | Payments


parser : List String -> Parser (Route -> a) a
parser publicPath =
    let
        parsePublicPath =
            List.foldl
                (\fragment parserSoFar -> parserSoFar </> s fragment)
                Parser.top
                publicPath
    in
    parsePublicPath
        </> Parser.oneOf
                [ Parser.map Top Parser.top
                , Parser.map Login (s "login")
                , Parser.map ChooseFlat (s "choose-flat")
                , Parser.map Flat
                    (s "flat"
                        </> Parser.string
                        </> Parser.oneOf
                                [ Parser.map Overview (s "overview")
                                , Parser.map Payments (s "payments")
                                ]
                    )
                ]


parse : Info -> Url -> Maybe Route
parse info =
    Parser.parse (parser info.publicPath)


replaceUrl : Info -> Route -> Cmd msg
replaceUrl =
    runNavigation Navigation.replaceUrl


pushUrl : Info -> Route -> Cmd msg
pushUrl =
    runNavigation Navigation.pushUrl


runNavigation : (Navigation.Key -> String -> Cmd msg) -> Info -> Route -> Cmd msg
runNavigation navigationFunction info route =
    navigationFunction info.navigationKey
        (publicPathToString info.publicPath ++ toString route)


publicPathToString : List String -> String
publicPathToString path =
    String.concat (List.map ((++) "/") path)


toHref : Route -> String
toHref route =
    toString route


toString : Route -> String
toString route =
    case route of
        Top ->
            "/"

        Login ->
            "/login"

        ChooseFlat ->
            "/choose-flat"

        Flat groupId flatRoute ->
            "/flat/" ++ groupId ++ flatRouteToString flatRoute


flatRouteToString : FlatRoute -> String
flatRouteToString flatRoute =
    case flatRoute of
        Overview ->
            "/overview"

        Payments ->
            "/payments"


filterExternal : Info -> Browser.UrlRequest -> Browser.UrlRequest
filterExternal info request =
    let
        publicPath =
            "/" ++ publicPathToString info.publicPath

        stripPublicPath urlPath =
            if String.startsWith publicPath urlPath then
                Just (String.dropLeft (String.length publicPath) urlPath)

            else
                Nothing

        filterOutsidePublicPath url =
            case stripPublicPath url.path of
                Just stripped ->
                    Browser.Internal { url | path = "/" ++ stripped }

                Nothing ->
                    Browser.External (Url.toString url)
    in
    case request of
        Browser.External urlString ->
            Browser.External urlString

        Browser.Internal url ->
            filterOutsidePublicPath url
