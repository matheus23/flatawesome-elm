import '@babel/polyfill';
import Kinto from 'kinto';
import Route from 'route-parser';

const recordRequests = "/v1/buckets/:bucket/collections/:collection/records";
const recordsRoute = new Route(recordRequests);

let cache = null;

self.addEventListener('install', event => {
    console.log('ServiceWorker installed');

    if (serviceWorkerOption.enableCache) {
        event.waitUntil(initializeCache());
    }
});

async function initializeCache() {
    cache = await caches.open('static-v1');
    return await cache.addAll(serviceWorkerOption.assets);
}

self.addEventListener('activate', event => {
    console.log('ServiceWorker activated');
});

self.addEventListener('fetch', event => {
    const url = new URL(event.request.url);

    const parsedRoute = recordsRoute.match(url.pathname);
    if (parsedRoute) {
        // TODO: Measure performance of .collection and new Kinto and consider memoizing
        const kinto = new Kinto({
            remote: url.origin + "/v1",
            bucket: parsedRoute.bucket
        });
        const collection = kinto.collection(parsedRoute.collection);

        event.respondWith(translateKintoRequestToResponse(url, collection, event.request));
    } else if (
        serviceWorkerOption.enableCache &&
        url.origin === location.origin &&
        event.request.method === 'GET'
    ) { // we only cache local get requests
        if (serviceWorkerOption.assets.includes(url.pathname)) {
            console.log('responding to asset from cache')
            event.respondWith(tryToRespondFromCache(event.request, url.pathname));
        } else {
            console.log('some other url requested, responding with index.html', url.toString());
            event.respondWith(tryToRespondFromCache(event.request, "/index.html"));
        }
    }
    // TODO: Somehow cache/handle google fonts... probably self-serve?
});

async function translateKintoRequestToResponse(url, collection, request) {
    const trySyncing = async () => {
        try {
            await collection.sync({
                headers: {
                    'Authorization': request.headers.get('Authorization')
                }
            });
        } catch (syncError) {
            console.warn('We\'re seemingly offline:', syncError);
        }
    };

    const jsonResponse = (additionalHeaders, json) => new Response(JSON.stringify(json), {
        status: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Expose-Headers': Object.keys(additionalHeaders).join(', '),
            'Content-Type': 'application/json',
            ...additionalHeaders
        }
    });

    try {
        switch (request.method) {
            case 'GET':
                await trySyncing(); // wait for an attempted sync, in case we're online
                
                let order = undefined;
                let filters = {};
                
                for (let [key, value] of url.searchParams) {
                    if (key === '_sort') {
                        order = value;
                        continue;
                    }

                    try {
                        // for stuff like ?userId=[1, 2, 3] or ?name="quotedName"
                        const jsonFilterSpec = JSON.parse(value);
                        filters[key] = jsonFilterSpec;
                    } catch (syntaxError) {
                        // for a default fallback, that is ommitting quotes for strings
                        filters[key] = value;
                    }
                }

                const dataAndPermissions = await collection.list({
                    order: order,
                    filters: filters
                });

                return jsonResponse(
                    { 'Total-Records': dataAndPermissions.data.length },
                    dataAndPermissions);
            case 'POST':
                const newRecord = await request.json();

                if (newRecord.data == null) {
                    // cause a 400 - Bad Request
                    throw new Error('Expected something with .data in the request body, instead got ' + newRecord);
                }

                const answer = await collection.create(newRecord.data);
                
                trySyncing(); // don't wait for this, let this go async

                return jsonResponse({}, answer);
        }
    } catch (error) {
        return new Response(error, {
            status: 400,
            headers: {
                'Content-Type': 'text/plain'
            }
        });
    }
}

async function tryToRespondFromCache(request, pathname) {
    const cacheMiss = () => {
        console.log('cache miss', pathname);
        return fetch(request)
    };

    try {
        const cachedResponse = await caches.match(pathname);
        
        if (cachedResponse == null) {
            return cacheMiss();
        }

        console.log('answered from cache', pathname);
        return cachedResponse;
    } catch (error) {
        return cacheMiss();
    }
}