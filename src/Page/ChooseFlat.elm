module Page.ChooseFlat exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Api
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Kinto
import Log
import Routes exposing (Route)
import Session
import Styles



-- Model


type alias Model =
    { credidentials : Session.Credidentials
    , step : Step
    }


type Step
    = Decide
    | CreatingFlatLoading


type Msg
    = CreateFlat
    | JoinFlat
    | GroupAdded (Result Kinto.Error Api.Group)


init : Session.Credidentials -> Model
init credidentials =
    { credidentials = credidentials
    , step = Decide
    }



-- Update


update : (Route -> Cmd Msg) -> Msg -> Model -> ( Model, Cmd Msg )
update pushUrl msg model =
    case ( model.step, msg ) of
        ( Decide, CreateFlat ) ->
            ( { model | step = CreatingFlatLoading }
            , Api.createFlatawesomeGroup GroupAdded model.credidentials
            )

        ( Decide, JoinFlat ) ->
            ( model, Cmd.none )

        ( Decide, _ ) ->
            ( model, Log.warning "Dropped message in Decide state" )

        ( CreatingFlatLoading, GroupAdded (Ok group) ) ->
            ( model
            , pushUrl (Routes.Flat group.id Routes.Overview)
            )

        ( CreatingFlatLoading, GroupAdded (Err error) ) ->
            -- TODO Handle error! Visually for the user
            ( model, Log.error (Kinto.errorToString error) )

        ( CreatingFlatLoading, _ ) ->
            ( model, Log.warning "Dropped message in CreatingFlatLoading state" )



-- View


view : Model -> StyledDocument Msg
view model =
    viewWithHeader
        "Choose a Flat | FlatAwesome"
        (case model.step of
            Decide ->
                [ viewDecidingStep ]

            CreatingFlatLoading ->
                [ text "Creating a space for you and your friends... (Loading)" ]
        )


viewDecidingStep : Html Msg
viewDecidingStep =
    let
        fullwidthButtonStyle =
            css
                [ Css.width (pct 100)
                , Css.height (px 120)
                , Css.margin (px 4)
                ]
    in
    section
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.width (pct 100)
            ]
        ]
        [ button
            [ onClick CreateFlat
            , fullwidthButtonStyle
            , Styles.buttonStyle Styles.green
            ]
            [ h2 [] [ text "Create a new Flat" ] ]
        , button
            [ onClick JoinFlat
            , fullwidthButtonStyle
            , Styles.buttonStyle Styles.blue
            ]
            [ h2 [] [ text "Join an existing Flat" ] ]
        ]
