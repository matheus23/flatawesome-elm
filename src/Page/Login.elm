module Page.Login exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Api
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Kinto
import Log
import Routes exposing (Route)
import Session exposing (Session)
import Styles



-- Model


type alias Model =
    { username : String
    , password : String
    , usernameError : Bool
    , passwordError : Bool
    , errorMessage : Maybe String
    , redirectInfo :
        Maybe
            { message : String
            , link : Route
            }
    }


getLoginInfo : { a | username : String, password : String } -> Api.LoginInfo
getLoginInfo { username, password } =
    { username = username, password = password }


isInvalid : Model -> Bool
isInvalid { usernameError, passwordError } =
    usernameError || passwordError


redirectInfoAlreadyLoggedIn : Route -> { message : String, link : Route }
redirectInfoAlreadyLoggedIn route =
    { message = "You're already logged in!", link = route }


init : Session.Session -> ( Model, Cmd Msg )
init session =
    let
        baseModel =
            { username = ""
            , password = ""
            , usernameError = False
            , passwordError = False
            , errorMessage = Nothing
            , redirectInfo = Nothing
            }
    in
    case session of
        Session.Unauthorized ->
            ( baseModel, Cmd.none )

        Session.Authorized creds ->
            ( baseModel
            , Api.fetchUserInfo UserInfoFetchedForRedirect creds
            )


type Msg
    = ChangedUsername String
    | ChangedPassword String
    | AttemptLogin
    | AttemptRegister
    | AccountRegistered (Result Kinto.Error Api.LoginInfo)
    | SignedUpForFlats (Result Kinto.Error Value)
    | LoginAttempted Api.LoginInfo (Result Http.Error Api.UserInfo)
    | UserInfoFetchedForRedirect (Result Http.Error Api.UserInfo)



-- Update


update : (Route -> Cmd Msg) -> Msg -> Model -> ( Model, Maybe Session, Cmd Msg )
update pushUrl msg model =
    case msg of
        ChangedUsername username ->
            ( { model | username = username }, Nothing, Cmd.none )

        ChangedPassword password ->
            ( { model | password = password }, Nothing, Cmd.none )

        UserInfoFetchedForRedirect (Ok userInfo) ->
            case Api.recognizeJoinedFlat userInfo of
                Just flatId ->
                    ( { model
                        | redirectInfo =
                            Just (redirectInfoAlreadyLoggedIn (Routes.Flat flatId Routes.Overview))
                      }
                    , Nothing
                    , Cmd.none
                    )

                Nothing ->
                    ( { model
                        | redirectInfo =
                            Just (redirectInfoAlreadyLoggedIn Routes.ChooseFlat)
                      }
                    , Nothing
                    , Cmd.none
                    )

        UserInfoFetchedForRedirect (Err error) ->
            ( { model
                -- TODO: Better error messages
                | errorMessage =
                    Just
                        ("Hmm. You seem to be logged in, but we couldn't "
                            ++ "find out minimal user details..."
                        )
              }
            , Nothing
            , Cmd.none
            )

        AttemptLogin ->
            let
                loginInfo =
                    getLoginInfo model

                creds =
                    Session.basicAuth loginInfo

                validatedModel =
                    validate model
            in
            if isInvalid validatedModel then
                ( validatedModel, Nothing, Cmd.none )

            else
                ( validatedModel
                , Nothing
                , Api.attemptLogin (LoginAttempted loginInfo) loginInfo creds
                )

        LoginAttempted user (Ok userInfo) ->
            let
                creds =
                    Session.basicAuth user

                joinedFlat =
                    Api.recognizeJoinedFlat userInfo

                session =
                    Session.authorize creds
            in
            ( { model
                | username = user.username
                , password = user.username
              }
            , Just session
            , case joinedFlat of
                Nothing ->
                    pushUrl Routes.ChooseFlat

                Just flatId ->
                    pushUrl (Routes.Flat flatId Routes.Overview)
            )

        LoginAttempted user (Err error) ->
            case error of
                Http.BadStatus response ->
                    case response.status.code of
                        -- 401 Unauthorized
                        401 ->
                            ( { model
                                | errorMessage = Just "Invalid username/password combination"
                              }
                            , Nothing
                            , Cmd.none
                            )

                        -- 403 Forbidden
                        403 ->
                            ( { model
                                | errorMessage = Just "We're sorry, this account name is already taken"
                              }
                            , Nothing
                            , Cmd.none
                            )

                        code ->
                            ( { model | errorMessage = Just ("Sorry something went wrong (" ++ String.fromInt code ++ ")") }
                            , Nothing
                            , Cmd.none
                            )

                anotherError ->
                    -- TODO: Handle other errors more specifically
                    ( { model | errorMessage = Just "Sorry, something went wrong" }
                    , Nothing
                    , case anotherError of
                        Http.BadPayload payloadDebugMsg _ ->
                            Log.error payloadDebugMsg

                        _ ->
                            Cmd.none
                    )

        AttemptRegister ->
            let
                validatedModel =
                    validate model
            in
            if isInvalid validatedModel then
                ( validatedModel, Nothing, Cmd.none )

            else
                ( model
                , Nothing
                , Api.attemptRegister AccountRegistered (getLoginInfo model)
                )

        AccountRegistered (Ok login) ->
            let
                creds =
                    Session.basicAuth login
            in
            ( model
            , Just (Session.authorize creds)
            , Api.signUpForFlats SignedUpForFlats login creds
            )

        AccountRegistered (Err error) ->
            kintoErrorUpdate error model

        SignedUpForFlats (Ok value) ->
            ( model
            , Nothing
            , Cmd.batch
                [ pushUrl Routes.ChooseFlat
                , Log.value "SugnedUpForFlats answer " value
                ]
            )

        SignedUpForFlats (Err error) ->
            kintoErrorUpdate error model


kintoErrorUpdate : Kinto.Error -> Model -> ( Model, Maybe Session, Cmd Msg )
kintoErrorUpdate error model =
    let
        errorMsg =
            Kinto.errorToString error
    in
    -- TODO: Improve error handling, use the error messages more precisely
    ( { model
        | errorMessage = Just errorMsg
      }
    , Nothing
    , Log.error errorMsg
    )


validate : Model -> Model
validate model =
    let
        { username, password } =
            getLoginInfo model
    in
    { model
        | usernameError = String.isEmpty username
        , passwordError = String.isEmpty password
    }



-- View


view : Model -> StyledDocument Msg
view model =
    viewWithHeader
        "Login | FlatAwesome"
        [ Html.Styled.form
            [ onSubmit AttemptLogin
            , Styles.flexCenter
            , css
                [ Css.flexDirection Css.column
                , Css.width Css.auto
                ]
            , Styles.boxStyle
            ]
            (List.concat
                [ [ h4 [] [ text "Log In :)" ]
                  , input
                        [ type_ "text"
                        , onInput ChangedUsername
                        , value model.username
                        , Styles.inputStyle model.usernameError
                        , css [ Css.margin (px 4) ]
                        , placeholder "Username"
                        , autocomplete True
                        ]
                        []
                  , input
                        [ type_ "password"
                        , onInput ChangedPassword
                        , value model.password
                        , Styles.inputStyle model.passwordError
                        , css [ Css.margin (px 4) ]
                        , placeholder "Password"
                        , autocomplete True
                        ]
                        []
                  ]
                , case model.errorMessage of
                    Just error ->
                        [ label [] [ p [ Styles.errorMessage ] [ text error ] ] ]

                    Nothing ->
                        []
                , case model.redirectInfo of
                    Nothing ->
                        []

                    Just { message, link } ->
                        [ p [ Styles.infoMessage ]
                            [ text (message ++ " "), a [ href (Routes.toHref link) ] [ text "Continue." ] ]
                        ]
                , [ input
                        [ type_ "submit"
                        , css [ Css.margin (px 4) ]
                        , Styles.buttonStyle Styles.green
                        , value "Log in"
                        ]
                        []
                  , button
                        [ type_ "button"
                        , onClick AttemptRegister
                        , css [ Css.margin (px 4) ]
                        , Styles.buttonStyle Styles.blue
                        ]
                        [ text "Register" ]
                  ]
                ]
            )
        ]
