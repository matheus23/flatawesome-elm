module Page.Top exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Api
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Http
import Loadable
import Log
import Routes exposing (Route)
import Session exposing (Session)



-- Model


type alias Model =
    { credidentials : Session.Credidentials }


type Msg
    = UserInfoFetched (Result Http.Error Api.UserInfo)


init : (Route -> Cmd Msg) -> Session -> ( Model, Cmd Msg )
init pushUrl session =
    case session of
        Session.Authorized creds ->
            ( { credidentials = creds }
            , Api.fetchUserInfo UserInfoFetched creds
            )

        Session.Unauthorized ->
            -- TODO: These empty creds are shit
            ( { credidentials = Session.basicAuth { username = "", password = "" } }
            , pushUrl Routes.Login
            )



-- Update


update : (Route -> Cmd Msg) -> Msg -> Model -> ( Model, Maybe Session, Cmd Msg )
update pushUrl msg model =
    case msg of
        UserInfoFetched (Ok userInfo) ->
            case Api.recognizeJoinedFlat userInfo of
                Just flatId ->
                    ( model
                    , Just (Session.Authorized model.credidentials)
                    , pushUrl (Routes.Flat flatId Routes.Overview)
                    )

                Nothing ->
                    ( model, Nothing, pushUrl Routes.ChooseFlat )

        -- TODO: Show user warning!
        UserInfoFetched (Err error) ->
            ( model
            , Nothing
            , Cmd.batch
                [ Log.warning "Error while loading userInfo"
                , pushUrl Routes.Login
                ]
            )



-- View


view : Model -> StyledDocument Msg
view model =
    viewWithHeader "FlatAwesome"
        [ text "Loading..." ]
