module Page.Flat.Overview exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

import Api
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Kinto
import Session
import Styles



-- Model


type alias Model =
    { credidentials : Session.Credidentials
    , group : Api.Group
    , usernameToAdd : String
    , error : Maybe String
    }


type Msg
    = UsernameToAddChanged String
    | AddUsername String
    | UsernameLookingForFlat String (Result Kinto.Error (Kinto.Pager String))
    | UsernameAdded (Result Kinto.Error Api.Group)
    | RemoveAccount String
    | AccountRemoved (Result Kinto.Error Api.Group)


init : Session.Credidentials -> Api.Group -> ( Model, Cmd Msg )
init credidentials group =
    ( { credidentials = credidentials
      , group = group
      , usernameToAdd = ""
      , error = Nothing
      }
    , Cmd.none
    )



-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        kintoError error =
            ( { model | error = Just (Kinto.errorToString error) }
            , Cmd.none
            )
    in
    case msg of
        UsernameToAddChanged usernameToAdd ->
            ( { model
                | usernameToAdd = usernameToAdd
                , error = Nothing
              }
            , Cmd.none
            )

        AddUsername username ->
            ( model
            , Api.fetchUsernameLookingForFlat (UsernameLookingForFlat username) username model.credidentials
            )

        UsernameLookingForFlat username (Ok pager) ->
            if List.isEmpty pager.objects then
                ( { model
                    | error = Just "Couldn't find a user with such username, who is not yet in a flat"
                  }
                , Cmd.none
                )

            else
                ( model
                , Api.addUsernameToGroup UsernameAdded username model.group model.credidentials
                )

        UsernameLookingForFlat username (Err error) ->
            kintoError error

        UsernameAdded (Ok group) ->
            ( { model
                | usernameToAdd = ""
                , group = group
                , error = Nothing
              }
            , Cmd.none
            )

        UsernameAdded (Err error) ->
            kintoError error

        RemoveAccount accountPrincipal ->
            ( model
            , Api.removeUsernameFromGroup AccountRemoved accountPrincipal model.group model.credidentials
            )

        AccountRemoved (Ok group) ->
            ( { model
                | group = group
                , error = Nothing
              }
            , Cmd.none
            )

        AccountRemoved (Err error) ->
            kintoError error



-- View


view : Model -> StyledDocument Msg
view model =
    viewWithHeader
        "Overview | FlatAwesome"
        [ viewContent model ]


viewContent : Model -> Html Msg
viewContent model =
    let
        invalid =
            case model.error of
                Just _ ->
                    True

                _ ->
                    False

        memberNotes =
            "You can add users to this flat here. Note that we assume that you all trust each other. "
                ++ "All flat members have the same rights. Everyone can invite or remove other flat members, "
                ++ "so don't add random usernames and make sure you remove accidentally invited users."
    in
    section []
        [ h3 [] [ text "Flat Members:" ]
        , p []
            [ text memberNotes
            ]
        , hr [] []
        , viewMembers model.group
        , Html.Styled.form [ onSubmit (AddUsername model.usernameToAdd) ]
            ([ input
                [ Styles.inputStyle invalid
                , placeholder "Username to add"
                , value model.usernameToAdd
                , onInput UsernameToAddChanged
                ]
                []
             , input
                [ type_ "submit"
                , value "Add User"
                , Styles.buttonStyle Styles.green
                , onClick (AddUsername model.usernameToAdd)
                ]
                []
             ]
                ++ (case model.error of
                        Just errorMsg ->
                            [ p [ Styles.errorMessage ] [ text errorMsg ] ]

                        Nothing ->
                            []
                   )
            )
        ]


viewMembers : Api.Group -> Html Msg
viewMembers group =
    let
        prefix =
            "account:"

        extractName =
            String.dropLeft (String.length prefix)

        viewMember accountPrincipal =
            li []
                [ text (extractName accountPrincipal)
                , div
                    [ css [ Css.float Css.right, Css.textAlign Css.right ]
                    , Styles.clickable
                    , onClick (RemoveAccount accountPrincipal)
                    ]
                    [ text "X" ]
                ]
    in
    ul []
        (List.map viewMember group.members)
