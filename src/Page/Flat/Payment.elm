module Page.Flat.Payment exposing
    ( Model
    , ModelData
    , add
    , decode
    , encodeData
    , recordResource
    , skeletonView
    , view
    )

import Avatar
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Json.Decode as Decode
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode
import Kinto
import ParseMoney
import Styles


type alias Model =
    { id : String
    , amount : Int
    , giver : String
    , lenders : List String
    , description : String
    }


type alias ModelData =
    { amount : Int
    , giver : String
    , lenders : List String
    , description : String
    }


decode : Decode.Decoder Model
decode =
    Decode.succeed Model
        |> Decode.required "id" Decode.string
        |> Decode.required "amount" Decode.int
        |> Decode.required "giver" Decode.string
        |> Decode.required "lenders" (Decode.list Decode.string)
        |> Decode.required "description" Decode.string


encodeData : ModelData -> Encode.Value
encodeData data =
    Encode.object
        [ ( "amount", Encode.int data.amount )
        , ( "giver", Encode.string data.giver )
        , ( "lenders", Encode.list Encode.string data.lenders )
        , ( "description", Encode.string data.description )
        ]


recordResource : Kinto.Resource Model
recordResource =
    Kinto.recordResource "flatawesome" "payments" decode


add : Kinto.Client -> (Model -> msg) -> (Kinto.Error -> msg) -> ModelData -> Cmd msg
add client handleSuccess handleError data =
    client
        |> Kinto.create recordResource (encodeData data)
        |> Kinto.send (Common.handleResult handleSuccess handleError)


skeletonView :
    List (Attribute msg)
    ->
        { viewAmount : Html msg
        , viewDescription : Html msg
        , viewGiver : Html msg
        , viewLender : Int -> lender -> Html msg
        }
    -> List lender
    -> Html msg
skeletonView additionalAttributes views lenders =
    let
        marginHoriz =
            4

        sectionStyling =
            css
                [ Css.margin2 zero (px marginHoriz)
                ]

        shrinkingSection =
            css
                [ Css.flexGrow zero
                , Css.flexShrink zero
                , Css.flexBasis Css.auto
                ]

        growingTruncatingSection =
            css
                [ Css.minWidth zero
                , Css.flex Css.auto
                ]
    in
    div
        (css
            [ Css.displayFlex
            , Css.alignItems Css.baseline
            , Css.margin2 zero (px -marginHoriz)
            , Css.position Css.relative
            ]
            :: additionalAttributes
        )
        [ section
            [ sectionStyling, shrinkingSection ]
            [ views.viewAmount, text "€" ]
        , section
            [ sectionStyling, growingTruncatingSection ]
            [ views.viewDescription ]
        , section
            [ sectionStyling, shrinkingSection ]
            [ views.viewGiver ]
        ]


view : (Int -> msg) -> Bool -> Int -> Model -> Html msg
view toggleExpansion isExpanded index payment =
    let
        viewExpander =
            div
                [ css
                    [ Css.left zero
                    , Css.right zero
                    , Css.height (px 12)
                    , Css.position Css.absolute
                    , Css.bottom (px 12)
                    , Css.textAlign Css.center
                    ]
                ]
                [ text "..." ]

        expandedContent =
            [ ul [ css [ Css.margin2 (px 4) zero ] ] (List.map Avatar.view payment.lenders) ]
    in
    li
        [ Styles.fineLineContainer
        , css
            [ Css.listStyle Css.none
            , Css.position Css.relative
            ]
        , Styles.clickable
        , onClick (toggleExpansion index)
        ]
        (skeletonView []
            { viewAmount = text (renderCents payment.amount)
            , viewDescription =
                div
                    (if isExpanded then
                        [ Styles.textWrapping ]

                     else
                        [ Styles.textTruncation ]
                    )
                    [ text payment.description ]
            , viewGiver = Avatar.view payment.giver
            , viewLender = \_ -> Avatar.view
            }
            payment.lenders
            :: (if isExpanded then
                    expandedContent

                else
                    [ viewExpander ]
               )
        )


renderCents : Int -> String
renderCents cents =
    let
        padded =
            String.padLeft 3 '0' (String.fromInt cents)

        splitAt index str =
            ( String.slice 0 index str, String.slice index (String.length str) str )

        ( left, right ) =
            splitAt -2 padded
    in
    left ++ "," ++ right
