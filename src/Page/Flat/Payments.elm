module Page.Flat.Payments exposing (Model, Msg, init, subscriptions, update, view)

import Avatar
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Kinto
import Page.Flat.Payment as Payment
import ParseMoney
import Styles


type alias Model =
    { payments : List Payment.Model
    , expandedIndex : Maybe Int
    , prototypeAmount : String
    , prototypeDescription : String
    , prototypeLenders : List ( Bool, String )
    }


init : Common.Flat -> ( Model, Cmd Msg )
init flat =
    ( { payments = []
      , expandedIndex = Nothing
      , prototypeAmount = ""
      , prototypeDescription = ""
      , prototypeLenders = initPrototypeLenders flat
      }
    , fetchPayments PaymentsFetched KintoError flat.client
    )


initPrototypeLenders : Common.Flat -> List ( Bool, String )
initPrototypeLenders flat =
    List.map (\mateThatsNotTheUser -> ( True, mateThatsNotTheUser ))
        (List.filter (\mate -> mate /= flat.user) flat.flatMates)


fetchPayments : (List Payment.Model -> msg) -> (Kinto.Error -> msg) -> Kinto.Client -> Cmd msg
fetchPayments handleSuccess handleError client =
    client
        |> Kinto.getList Payment.recordResource
        |> Kinto.sort [ "-last_modified" ]
        |> Kinto.send (Common.handleResult (.objects >> handleSuccess) handleError)


type Msg
    = AddPayment
    | ToggleAvatar Int
    | ChangePrototypeAmount String
    | ChangePrototypeDescription String
    | TogglePaymentExpansion Int
    | PaymentAdded Payment.Model
    | PaymentsFetched (List Payment.Model)
    | KintoError Kinto.Error
    | Refetch



-- Update


update : Common.Flat -> Msg -> Model -> ( Model, Cmd Msg )
update flat message model =
    case message of
        PaymentsFetched payments ->
            ( { model | payments = payments }, Cmd.none )

        AddPayment ->
            case prototypeToPaymentData flat model of
                Ok paymentData ->
                    ( { model
                        | prototypeAmount = ""
                        , prototypeDescription = ""
                        , prototypeLenders = initPrototypeLenders flat
                      }
                    , Payment.add flat.client PaymentAdded KintoError paymentData
                    )

                Err error ->
                    -- TODO: Error tooltips
                    ( model, Cmd.none )

        PaymentAdded payment ->
            ( { model | payments = payment :: model.payments }, Cmd.none )

        ToggleAvatar index ->
            let
                toggleAvatarIndex toggleIndex =
                    List.indexedMap
                        (\lenderIndex ( enabled, lendingUser ) ->
                            ( xor (toggleIndex == lenderIndex) enabled, lendingUser )
                        )
            in
            ( { model
                | prototypeLenders = toggleAvatarIndex index model.prototypeLenders
              }
            , Cmd.none
            )

        TogglePaymentExpansion index ->
            let
                toggledExpandedIndex =
                    case model.expandedIndex of
                        Just alreadyExpandedIndex ->
                            if alreadyExpandedIndex == index then
                                Nothing

                            else
                                Just index

                        Nothing ->
                            Just index
            in
            ( { model | expandedIndex = toggledExpandedIndex }, Cmd.none )

        ChangePrototypeAmount str ->
            ( { model | prototypeAmount = str }, Cmd.none )

        ChangePrototypeDescription str ->
            ( { model | prototypeDescription = str }, Cmd.none )

        KintoError err ->
            -- TODO: Error tooltips
            ( model, Cmd.none )

        Refetch ->
            ( model, fetchPayments PaymentsFetched KintoError flat.client )


prototypeToPaymentData : Common.Flat -> Model -> Result String Payment.ModelData
prototypeToPaymentData flat model =
    let
        validateNonEmpty string =
            if String.isEmpty string then
                Nothing

            else
                Just string
    in
    case
        ( ParseMoney.runMaybe ParseMoney.parseMoney model.prototypeAmount
        , validateNonEmpty model.prototypeDescription
        )
    of
        ( Just cents, Just description ) ->
            Result.Ok
                { amount = cents
                , giver = flat.user
                , lenders = List.map Tuple.second (List.filter Tuple.first model.prototypeLenders)
                , description = description
                }

        ( Nothing, _ ) ->
            Result.Err "Amount needs to be money (something like 10,00 or 23)"

        ( _, Nothing ) ->
            Result.Err "Description needs to be set"



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- View


view : Common.Flat -> Model -> StyledDocument Msg
view flat model =
    let
        isExpanded index =
            Maybe.withDefault False (Maybe.map (\i -> i == index) model.expandedIndex)
    in
    viewWithHeader
        "Payments | FlatAwesome"
        (viewAddPayment flat model
            :: button [ onClick Refetch, Styles.buttonStyle Styles.green ] [ text "Refetch" ]
            :: List.indexedMap
                (\index -> Payment.view TogglePaymentExpansion (isExpanded index) index)
                model.payments
        )


viewAddPayment : Common.Flat -> Model -> Html Msg
viewAddPayment flat model =
    let
        amountInput =
            input
                [ type_ "text"
                , value model.prototypeAmount
                , onInput ChangePrototypeAmount
                , Styles.inputStyle False
                , size 3
                ]
                []

        descriptionInput =
            input
                [ type_ "text"
                , value model.prototypeDescription
                , onInput ChangePrototypeDescription
                , Styles.inputStyle False
                , placeholder "Description"
                , css [ Css.width (pct 100) ]
                ]
                []

        noUserSelect =
            css
                [ Css.property "user-select" "none"
                , Css.property "-moz-user-select" "none"
                , Css.property "-webkit-user-select" "none"
                , Css.property "-ms-user-select" "none"
                ]

        viewAvatarToggleable index ( enabled, user ) =
            Avatar.skeletonView
                [ onClick (ToggleAvatar index)
                , Styles.clickable
                , noUserSelect
                ]
                [ input [ type_ "checkbox", checked enabled, css [ Css.margin2 zero (px 4) ] ] [] ]
                (if enabled then
                    ( Styles.green, Styles.grey )

                 else
                    ( Styles.grey, Styles.dark )
                )
                user

        marginVert =
            4
    in
    Html.Styled.form
        [ Styles.fineLineContainer
        , css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.margin2 (px -marginVert) zero
            ]
        , onSubmit AddPayment
        ]
        [ h4 [ css [ Css.margin zero ] ] [ text "Add a Payment:" ]
        , Payment.skeletonView
            [ css [ Css.margin2 (px marginVert) zero ] ]
            { viewAmount = amountInput
            , viewDescription = descriptionInput
            , viewGiver = Avatar.skeletonView [] [] ( Styles.green, Styles.grey ) flat.user
            , viewLender = viewAvatarToggleable
            }
            model.prototypeLenders
        , ul
            [ css [ Css.margin2 (px marginVert) zero ]
            ]
            (List.indexedMap viewAvatarToggleable model.prototypeLenders)
        , input
            [ type_ "submit"
            , value "Log Payment"
            , css
                [ Css.margin2 (px marginVert) zero
                , Css.padding (px 4)
                ]
            , Styles.buttonStyle Styles.green
            ]
            []
        ]
