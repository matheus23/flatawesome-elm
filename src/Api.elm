module Api exposing
    ( Group
    , LoginInfo
    , UserInfo
    , addUsernameToGroup
    , attemptLogin
    , attemptRegister
    , createFlatawesomeGroup
    , fetchGroup
    , fetchUserInfo
    , fetchUsernameLookingForFlat
    , recognizeJoinedFlat
    , removeUsernameFromGroup
    , signUpForFlats
    )

import Http
import HttpBuilder
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Kinto
import Session
import Task



-- Login


type alias LoginInfo =
    { username : String, password : String }


type alias UserInfo =
    { id : String
    , principals : List String
    , bucket : Maybe String
    }


attemptLogin :
    (Result Http.Error UserInfo -> msg)
    -> LoginInfo
    -> Session.Credidentials
    -> Cmd msg
attemptLogin produceMessage login credidentials =
    let
        kintoClient =
            Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth

        requestLogin =
            kintoClient
                |> Kinto.get (Kinto.accountsResource (Decode.succeed ())) login.username
                |> HttpBuilder.toTask

        tasks =
            requestLogin
                |> Task.andThen (\result -> HttpBuilder.toTask (requestUserInfo credidentials))
    in
    Task.attempt produceMessage tasks


requestUserInfo : Session.Credidentials -> HttpBuilder.RequestBuilder UserInfo
requestUserInfo credidentials =
    let
        kintoClient =
            Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth
    in
    HttpBuilder.get "http://localhost:8888/v1/"
        |> HttpBuilder.withHeaders kintoClient.headers
        |> HttpBuilder.withExpectJson (Decode.field "user" decodeUserInfo)


fetchUserInfo : (Result Http.Error UserInfo -> msg) -> Session.Credidentials -> Cmd msg
fetchUserInfo produceMessage credidentials =
    requestUserInfo credidentials
        |> HttpBuilder.send produceMessage


decodeUserInfo : Decoder UserInfo
decodeUserInfo =
    Decode.succeed UserInfo
        |> Decode.required "id" Decode.string
        |> Decode.required "principals" (Decode.list Decode.string)
        |> Decode.optional "bucket" (Decode.map Just Decode.string) Nothing


recognizeJoinedFlat : UserInfo -> Maybe String
recognizeJoinedFlat { principals } =
    let
        flatAwesomeGroupPrefix =
            "/buckets/flatawesome/groups/"
    in
    principals
        |> List.filter (String.startsWith flatAwesomeGroupPrefix)
        |> List.head
        |> Maybe.map (String.dropLeft (String.length flatAwesomeGroupPrefix))



-- TODO: attemptRegister and signUpForFlats should become one transaction!


attemptRegister : (Result Kinto.Error LoginInfo -> msg) -> LoginInfo -> Cmd msg
attemptRegister produceMessage login =
    Kinto.client "http://localhost:8888/v1" Kinto.NoAuth
        |> Kinto.create (Kinto.accountsResource (Decode.succeed login))
            (Encode.object
                [ ( "id", Encode.string login.username )
                , ( "password", Encode.string login.password )
                ]
            )
        |> Kinto.send produceMessage


signUpForFlats : (Result Kinto.Error Value -> msg) -> LoginInfo -> Session.Credidentials -> Cmd msg
signUpForFlats produceMessage login creds =
    Kinto.client "http://localhost:8888/v1" creds.kintoAuth
        |> Kinto.createWithPermissions (Kinto.recordResource "flatawesome" "looking_for_flat" Decode.value)
            (Encode.object
                [ ( "userId", Encode.string login.username ) ]
            )
            (Encode.object
                [ ( "write", Encode.list Encode.string [ "account:" ++ login.username ] )
                , ( "read", Encode.list Encode.string [ "System.Authenticated" ] )
                ]
            )
        |> Kinto.send produceMessage



-- Groups


type alias Group =
    { id : String
    , lastModified : Int
    , members : List String
    }


decodeGroup : Decoder Group
decodeGroup =
    Decode.succeed Group
        |> Decode.required "id" Decode.string
        |> Decode.required "last_modified" Decode.int
        |> Decode.required "members" (Decode.list Decode.string)


createFlatawesomeGroup : (Result Kinto.Error Group -> msg) -> Session.Credidentials -> Cmd msg
createFlatawesomeGroup produceMessage creds =
    Kinto.client "http://localhost:8888/v1" creds.kintoAuth
        |> Kinto.create (Kinto.groupResource "flatawesome" decodeGroup) (Encode.object [])
        |> Kinto.send produceMessage


addUsernameToGroup : (Result Kinto.Error Group -> msg) -> String -> Group -> Session.Credidentials -> Cmd msg
addUsernameToGroup produceMessage username group credidentials =
    Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth
        |> Kinto.update (Kinto.groupResource "flatawesome" decodeGroup)
            group.id
            (Encode.object
                [ ( "members"
                  , Encode.list Encode.string
                        (("account:" ++ username)
                            :: group.members
                        )
                  )
                ]
            )
        |> Kinto.send produceMessage


removeUsernameFromGroup : (Result Kinto.Error Group -> msg) -> String -> Group -> Session.Credidentials -> Cmd msg
removeUsernameFromGroup produceMessage accountPrincipal group credidentials =
    Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth
        |> Kinto.update (Kinto.groupResource "flatawesome" decodeGroup)
            group.id
            (Encode.object
                [ ( "members"
                  , Encode.list Encode.string
                        (List.filter ((/=) accountPrincipal) group.members)
                  )
                ]
            )
        |> Kinto.send produceMessage


fetchGroup : (Result Kinto.Error Group -> msg) -> String -> Session.Credidentials -> Cmd msg
fetchGroup produceMessage groupId credidentials =
    Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth
        |> Kinto.get (Kinto.groupResource "flatawesome" decodeGroup) groupId
        |> Kinto.send produceMessage


fetchUsernameLookingForFlat : (Result Kinto.Error (Kinto.Pager String) -> msg) -> String -> Session.Credidentials -> Cmd msg
fetchUsernameLookingForFlat produceMessage username credidentials =
    Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth
        |> Kinto.getList (Kinto.recordResource "flatawesome" "looking_for_flat" (Decode.field "userId" Decode.string))
        |> Kinto.filter (Kinto.EQUAL "userId" username)
        |> Kinto.send produceMessage
