module Avatar exposing (skeletonView, view)

import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import ParseMoney
import Styles


view : String -> Html msg
view =
    skeletonView [] [] ( Styles.grey, Styles.dark )


skeletonView : List (Attribute msg) -> List (Html msg) -> ( Css.Color, Css.Color ) -> String -> Html msg
skeletonView additionalAttributes additionalHtml ( color, textColor ) name =
    span
        (css
            [ Css.backgroundColor color
            , Css.padding (px 4)
            , Css.margin (px 4)
            , Css.color textColor
            ]
            :: additionalAttributes
        )
        (text name :: additionalHtml)
