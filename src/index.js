import '@babel/polyfill';
import 'whatwg-fetch';
import './global_styles.scss';

import './sw.js'; // this way we also get compile errors in our console, thats quite nice
import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import { Elm } from './Main';


const getStorageJson = (key) => {
    try {
        return JSON.parse(localStorage.getItem(key));
    } catch (e) {
        return null;
    }
};

const flags = {
    storedCredidentials: getStorageJson('flatawesome-session'),
    publicPath: PUBLIC_PATH
};
const app = Elm.Main.init({ flags });

const subscribe = (port, listener) => {
    if (port == null) {
        return;
    }
    port.subscribe(listener);
};

subscribe(app.ports.localStore, async ([key, value]) => {
    if (value == null) {
        localStorage.removeItem(key);
    } else {
        localStorage.setItem(key, JSON.stringify(value));
    }
});
subscribe(app.ports.log, async string => console.log(string));
subscribe(app.ports.logInfo, async string => console.info(string));
subscribe(app.ports.logWarning, async string => console.warn(string));
subscribe(app.ports.logError, async string => console.error(string));
subscribe(app.ports.logValue, async ([description, value]) => console.log(description, value));

(async () => {
    if ('serviceWorker' in navigator) {
        const registration = await runtime.register({
            //scope: "/v1" // just doesn't work.
        });
        console.log('loaded service worker');

        if (module.hot) {
            module.hot.accept('./sw.js', () => {
                console.log('updating service worker in a sec...');
                setTimeout(() => {
                    console.log('updating service worker!');
                    registration.update();  
                }, 1000);
            });
        }
    }
})();
