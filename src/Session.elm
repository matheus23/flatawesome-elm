module Session exposing
    ( Credidentials
    , Session(..)
    , authorize
    , basicAuth
    , decode
    , store
    )

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode
import Json.Encode as Encode exposing (Value)
import Kinto
import Ports


type Session
    = Authorized Credidentials
    | Unauthorized


type alias Credidentials =
    { username : String
    , kintoAuth : Kinto.Auth
    }


basicAuth : { username : String, password : String } -> Credidentials
basicAuth creds =
    { username = creds.username
    , kintoAuth = Kinto.Basic creds.username creds.password
    }


authorize : Credidentials -> Session
authorize creds =
    Authorized creds


store : Credidentials -> Cmd msg
store creds =
    Ports.localStore
        ( "flatawesome-session"
        , Just (encode creds)
        )


encode : Credidentials -> Value
encode { username, kintoAuth } =
    Encode.object
        [ ( "username", Encode.string username )
        , ( "kintoAuth", encodeAuth kintoAuth )
        ]


encodeAuth : Kinto.Auth -> Value
encodeAuth auth =
    case auth of
        Kinto.NoAuth ->
            Encode.null

        Kinto.Basic username password ->
            Encode.object
                [ ( "type", Encode.string "basic" )
                , ( "username", Encode.string username )
                , ( "password", Encode.string password )
                ]

        Kinto.Bearer token ->
            Encode.object
                [ ( "type", Encode.string "bearer" )
                , ( "token", Encode.string token )
                ]

        Kinto.Custom realm token ->
            Encode.object
                [ ( "type", Encode.string "custom" )
                , ( "realm", Encode.string realm )
                , ( "token", Encode.string token )
                ]


decode : Decoder Credidentials
decode =
    Decode.map2 Credidentials
        (Decode.field "username" Decode.string)
        (Decode.field "kintoAuth" decodeAuth)


decodeAuth : Decoder Kinto.Auth
decodeAuth =
    Decode.field "type" Decode.string
        |> Decode.andThen
            (\typeTag ->
                case typeTag of
                    "basic" ->
                        Decode.succeed Kinto.Basic
                            |> Decode.required "username" Decode.string
                            |> Decode.required "password" Decode.string

                    "bearer" ->
                        Decode.succeed Kinto.Bearer
                            |> Decode.required "token" Decode.string

                    "custom" ->
                        Decode.succeed Kinto.Custom
                            |> Decode.required "realm" Decode.string
                            |> Decode.required "token" Decode.string

                    unknownType ->
                        Decode.fail ("'type' tag should be one of 'basic', 'bearer' or 'custom'. Got '" ++ unknownType ++ "' instead.")
            )
        |> Decode.nullable
        |> Decode.map (Maybe.withDefault Kinto.NoAuth)
