module Styles exposing
    ( blue
    , boxStyle
    , buttonStyle
    , clickable
    , dark
    , errorMessage
    , fineLineContainer
    , flexCenter
    , green
    , grey
    , infoMessage
    , inputStyle
    , textTruncation
    , textWrapping
    , white
    )

import Css exposing (..)
import Css.Transitions exposing (easeInOut, transition)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)


white : Color
white =
    rgb 250 250 250


grey : Color
grey =
    rgb 230 230 230


dark : Color
dark =
    rgb 85 85 85


red : Color
red =
    rgb 216 110 82


green : Color
green =
    rgb 81 193 81


blue : Color
blue =
    rgb 82 120 206


pink : Color
pink =
    rgb 240 100 145


buttonStyle : Color -> Attribute msg
buttonStyle mainColor =
    css
        [ border3 (px 2.5) solid mainColor
        , backgroundColor white
        , color mainColor
        , transition
            [ Css.Transitions.backgroundColor2 100 0
            , Css.Transitions.color2 100 0
            , Css.Transitions.borderColor2 100 0
            ]
        , hover
            [ backgroundColor mainColor
            , color white
            ]
        , active
            [ backgroundColor mainColor
            , color white
            ]
        ]


boxStyle : Attribute msg
boxStyle =
    css
        [ Css.backgroundColor white
        , Css.border3 (px 1) Css.solid dark
        , Css.margin2 (px 12) zero
        , Css.padding (px 10)
        ]


fineLineContainer : Attribute msg
fineLineContainer =
    css
        [ borderBottom3 (px 1) solid grey
        , padding (px 6)
        ]


flexCenter : Attribute msg
flexCenter =
    css
        [ displayFlex
        , justifyContent center
        , alignItems center
        ]


inputStyle : Bool -> Attribute msg
inputStyle hasError =
    css
        [ backgroundColor (rgba 230 230 230 0.4)
        , border zero
        , borderBottom3
            (if hasError then
                px 3

             else
                px 1
            )
            solid
            (if hasError then
                red

             else
                dark
            )
        ]


clickable : Attribute msg
clickable =
    css
        [ cursor pointer ]


textTruncation : Attribute msg
textTruncation =
    css
        [ textOverflow ellipsis
        , overflow Css.hidden
        , whiteSpace noWrap
        ]


textWrapping : Attribute msg
textWrapping =
    css
        [ overflowWrap breakWord ]


errorMessage : Attribute msg
errorMessage =
    css
        [ color red ]


infoMessage : Attribute msg
infoMessage =
    css [ color blue ]
