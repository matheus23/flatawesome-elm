module Log exposing
    ( error
    , info
    , string
    , value
    , warning
    )

import Json.Encode as Encode exposing (Value)
import Ports


string : String -> Cmd msg
string =
    Ports.log


warning : String -> Cmd msg
warning =
    Ports.logWarning


info : String -> Cmd msg
info =
    Ports.logInfo


error : String -> Cmd msg
error =
    Ports.logError


value : String -> Value -> Cmd msg
value description valueToLog =
    Ports.logValue ( description, valueToLog )
