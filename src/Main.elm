module Main exposing (Model, Msg(..), init, main, update, view)

import Api
import Avatar
import Browser
import Browser.Navigation as Navigation
import Common
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)
import Kinto
import Loadable
import Page.ChooseFlat as ChooseFlat
import Page.Flat.Overview as Overview
import Page.Flat.Payments as Payments
import Page.Login as Login
import Page.Top as Top
import ParseMoney
import Ports
import Routes exposing (Route)
import Session exposing (Session)
import Styles
import Url exposing (Url)



-- Model


type alias Model =
    { routingInfo : Routes.Info
    , session : Session
    , page : Page
    }


type Page
    = TopPage Top.Model
    | LoginPage Login.Model
    | PaymentsPage Common.Flat Payments.Model
    | OverviewPage (Loadable.Model Overview.Model)
    | ChooseFlatPage ChooseFlat.Model
    | NotFound


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url navKey =
    let
        publicPath =
            List.filter ((/=) "") (String.split "/" flags.publicPath)

        routingInfo =
            { navigationKey = navKey
            , publicPath = publicPath
            }

        session =
            case parseCredidentialsFlag flags.storedCredidentials of
                Just creds ->
                    Session.Authorized creds

                Nothing ->
                    Session.Unauthorized
    in
    case Routes.parse routingInfo url of
        Just route ->
            { routingInfo = routingInfo
            , session = session

            -- TODO: This is a weird default, just so we can call "setPageFromRoute"
            -- maybe "setPageFromRoute" shouldn't _change_ the page, but instead choose one?
            , page = NotFound
            }
                |> setPageFromRoute route

        Nothing ->
            ( { routingInfo = routingInfo
              , session = session
              , page = NotFound
              }
            , Cmd.none
            )


parseCredidentialsFlag : Maybe Value -> Maybe Session.Credidentials
parseCredidentialsFlag maybeCredidentials =
    maybeCredidentials
        |> Maybe.andThen
            (\credidentialsValue ->
                Result.toMaybe (Decode.decodeValue Session.decode credidentialsValue)
            )


type Msg
    = TopMsg Top.Msg
    | LoginMsg Login.Msg
    | PaymentsMsg Payments.Msg
    | OverviewMsg (Loadable.Msg Api.Group Overview.Msg)
    | ChooseFlatMsg ChooseFlat.Msg
    | UrlRequest Browser.UrlRequest
    | UrlChange Url



-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case ( message, model.page ) of
        ( TopMsg msg, TopPage topModel ) ->
            let
                ( nextTopModel, session, topCommands ) =
                    Top.update
                        (Routes.pushUrl model.routingInfo)
                        msg
                        topModel
            in
            ( { model
                | session = Maybe.withDefault model.session session
                , page = TopPage nextTopModel
              }
            , Cmd.map TopMsg topCommands
            )

        ( LoginMsg msg, LoginPage loginModel ) ->
            let
                ( nextLoginModel, session, loginCommands ) =
                    Login.update
                        (Routes.pushUrl model.routingInfo)
                        msg
                        loginModel
            in
            ( { model
                | page = LoginPage nextLoginModel
                , session = Maybe.withDefault model.session session
              }
            , Cmd.batch
                [ Cmd.map LoginMsg loginCommands
                , case session of
                    Just (Session.Authorized credidentials) ->
                        Session.store credidentials

                    _ ->
                        Cmd.none
                ]
            )

        ( PaymentsMsg msg, PaymentsPage flat paymentsModel ) ->
            let
                ( newPaymentsModel, paymentsCmds ) =
                    Payments.update flat msg paymentsModel
            in
            ( { model | page = PaymentsPage flat newPaymentsModel }
            , Cmd.map PaymentsMsg paymentsCmds
            )

        ( ChooseFlatMsg msg, ChooseFlatPage chooseFlatModel ) ->
            let
                ( nextChooseFlatModel, chooseFlatCommands ) =
                    ChooseFlat.update
                        (Routes.pushUrl model.routingInfo)
                        msg
                        chooseFlatModel
            in
            ( { model | page = ChooseFlatPage nextChooseFlatModel }
            , Cmd.map ChooseFlatMsg chooseFlatCommands
            )

        ( OverviewMsg msg, OverviewPage loadableModel ) ->
            case model.session of
                Session.Authorized credidentials ->
                    let
                        config =
                            { update = Overview.update
                            , initOnLoaded = \group -> Overview.init credidentials group
                            }

                        ( nextLoadableModel, commands ) =
                            Loadable.update config msg loadableModel
                    in
                    ( { model | page = OverviewPage nextLoadableModel }
                    , Cmd.map OverviewMsg commands
                    )

                Session.Unauthorized ->
                    ( model, Routes.pushUrl model.routingInfo Routes.Login )

        ( UrlRequest to, _ ) ->
            case Routes.filterExternal model.routingInfo to of
                Browser.External urlString ->
                    ( model, Navigation.load urlString )

                Browser.Internal url ->
                    ( model, Navigation.pushUrl model.routingInfo.navigationKey (Url.toString url) )

        ( UrlChange url, _ ) ->
            Routes.parse model.routingInfo url
                |> Maybe.map (\route -> setPageFromRoute route model)
                |> Maybe.withDefault ( { model | page = NotFound }, Cmd.none )

        ( otherMsg, otherModel ) ->
            ( model, Cmd.none )


setPageFromRoute : Route -> Model -> ( Model, Cmd Msg )
setPageFromRoute route model =
    case ( route, model.session ) of
        ( Routes.Top, session ) ->
            let
                ( initModel, initCommands ) =
                    Top.init (Routes.pushUrl model.routingInfo) session
            in
            ( { model | page = TopPage initModel }
            , Cmd.map TopMsg initCommands
            )

        ( Routes.Login, session ) ->
            let
                ( loginModel, commands ) =
                    Login.init session
            in
            ( { model | page = LoginPage loginModel }
            , Cmd.map LoginMsg commands
            )

        ( Routes.Flat groupId flatRoute, Session.Authorized credidentials ) ->
            case flatRoute of
                Routes.Overview ->
                    ( { model
                        | page = OverviewPage Loadable.init
                      }
                    , Api.fetchGroup
                        (Common.handleResult
                            Loadable.signalLoaded
                            (Kinto.errorToString >> Loadable.signalError)
                        )
                        groupId
                        credidentials
                        |> Cmd.map OverviewMsg
                    )

                Routes.Payments ->
                    let
                        flat =
                            { user = credidentials.username
                            , flatMates = [ "Daniel", "Lisa" ]
                            , client = Kinto.client "http://localhost:8888/v1" credidentials.kintoAuth
                            }

                        ( initPaymentsPage, paymentsCmds ) =
                            Payments.init flat
                    in
                    ( { model | page = PaymentsPage flat initPaymentsPage }
                    , Cmd.map PaymentsMsg paymentsCmds
                    )

        ( Routes.Flat groupId flatRoute, Session.Unauthorized ) ->
            ( model
            , Routes.pushUrl model.routingInfo Routes.Login
            )

        ( Routes.ChooseFlat, Session.Authorized credidentials ) ->
            ( { model | page = ChooseFlatPage (ChooseFlat.init credidentials) }
            , Cmd.none
            )

        ( Routes.ChooseFlat, Session.Unauthorized ) ->
            ( model
            , Routes.pushUrl model.routingInfo Routes.Login
            )



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.page of
        TopPage _ ->
            Sub.none

        LoginPage _ ->
            Sub.none

        PaymentsPage flat paymentsModel ->
            Sub.map PaymentsMsg (Payments.subscriptions paymentsModel)

        OverviewPage _ ->
            Sub.none

        ChooseFlatPage chooseFlatModel ->
            Sub.none

        NotFound ->
            Sub.none



-- View


view : Model -> StyledDocument Msg
view model =
    case model.page of
        TopPage topModel ->
            mapDocument TopMsg (Top.view topModel)

        LoginPage loginModel ->
            mapDocument LoginMsg (Login.view loginModel)

        PaymentsPage flat paymentsModel ->
            mapDocument PaymentsMsg (Payments.view flat paymentsModel)

        OverviewPage overviewModel ->
            mapDocument OverviewMsg (Loadable.view Overview.view overviewModel)

        ChooseFlatPage chooseFlatModel ->
            mapDocument ChooseFlatMsg (ChooseFlat.view chooseFlatModel)

        NotFound ->
            viewWithHeader
                "Not Found | FlatAwesome"
                [ text "I have no Idea how you ended up here" ]



--


type alias Flags =
    { storedCredidentials : Maybe Value
    , publicPath : String
    }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view >> toUnstyledDocument
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequest
        , onUrlChange = UrlChange
        }
