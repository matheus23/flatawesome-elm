module Common exposing
    ( Flat
    , User
    , handleResult
    )

import Kinto


type alias User =
    String


type alias Flat =
    { user : User
    , flatMates : List String
    , client : Kinto.Client
    }


handleResult : (success -> msg) -> (error -> msg) -> Result error success -> msg
handleResult handleSuccess handleError result =
    case result of
        Ok model ->
            handleSuccess model

        Err error ->
            handleError error
