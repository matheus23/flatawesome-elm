module ParseMoney exposing (parseMoney, runMaybe, singleDigit)

import Parser exposing (..)


runMaybe : Parser a -> String -> Maybe a
runMaybe parser =
    Result.toMaybe << run parser


parseMoney : Parser Int
parseMoney =
    succeed (\euros cents -> euros * 100 + cents)
        |= int
        |= oneOf
            [ succeed (\centsTenths cents -> centsTenths * 10 + cents)
                |. symbol ","
                |= singleDigit
                |= oneOf
                    [ succeed identity
                        |= singleDigit
                        |. end
                    , succeed 0
                        |. end
                    ]
            , succeed 0
                |. end
            ]


singleDigit : Parser Int
singleDigit =
    let
        parseDigit digit =
            succeed (\_ -> digit)
                |= symbol (String.fromInt digit)
    in
    oneOf (List.map parseDigit (List.range 0 9))
