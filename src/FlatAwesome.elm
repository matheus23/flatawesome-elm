module FlatAwesome exposing
    ( StyledDocument
    , contentWrapper
    , mapDocument
    , toUnstyledDocument
    , viewWithHeader
    )

import Browser
import Css exposing (num, pct, px, rgb, rgba, zero)
import Css.Media as Media exposing (only, screen, withMedia)
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)


type alias StyledDocument msg =
    { title : String
    , body : Html msg -- Only one element, so we only have one .toUnstyled call
    }


toUnstyledDocument : StyledDocument msg -> Browser.Document msg
toUnstyledDocument { title, body } =
    { title = title, body = [ toUnstyled body ] }


mapDocument : (a -> msg) -> StyledDocument a -> StyledDocument msg
mapDocument toTopLevelMsg { title, body } =
    { title = title
    , body = Html.Styled.map toTopLevelMsg body
    }


viewWithHeader : String -> List (Html msg) -> StyledDocument msg
viewWithHeader title body =
    { title = title
    , body =
        contentWrapper
            (h1
                [ css [ Css.textAlign Css.center ]
                ]
                [ text "FlatAWESOME" ]
                -- THIS IS NOT OBVIOUS elm-format! :D elems sits in the list _after_ h1
                :: body
            )
    }


contentWrapper : List (Html msg) -> Html msg
contentWrapper content =
    div
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.justifyContent Css.center
            , Css.alignItems Css.center
            , Css.padding2 (px 5) (px 12)
            , Css.width (pct 100)
            ]
        ]
        [ main_
            [ css
                [ Css.maxWidth (px 960)
                , Css.width (pct 100)
                ]
            ]
            content
        ]
