module Loadable exposing
    ( Config
    , Model
    , Msg
    , init
    , signalError
    , signalLoaded
    , update
    , view
    )

import FlatAwesome exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)



-- Model


type Model m
    = Loading
    | Error String
    | Done m


type Msg l i
    = Loaded l
    | Errored String
    | InnerMsg i


signalLoaded : l -> Msg l i
signalLoaded =
    Loaded


signalError : String -> Msg l i
signalError =
    Errored


init : Model m
init =
    Loading



-- Update


type alias Config l i m =
    { update : i -> m -> ( m, Cmd i )
    , initOnLoaded : l -> ( m, Cmd i )
    }


update : Config l i m -> Msg l i -> Model m -> ( Model m, Cmd (Msg l i) )
update config msg model =
    case ( msg, model ) of
        ( InnerMsg innerMsg, Done innerModel ) ->
            let
                ( nextInnerModel, commands ) =
                    config.update innerMsg innerModel
            in
            ( Done nextInnerModel, Cmd.map InnerMsg commands )

        ( Loaded loadable, Loading ) ->
            let
                ( loadedInner, commands ) =
                    config.initOnLoaded loadable
            in
            ( Done loadedInner, Cmd.map InnerMsg commands )

        ( Errored errorMsg, Loading ) ->
            ( Error errorMsg, Cmd.none )

        ( _, _ ) ->
            ( model, Cmd.none )



-- View


view : (m -> StyledDocument i) -> Model m -> StyledDocument (Msg l i)
view viewInner model =
    case model of
        Loading ->
            viewWithHeader "Loading | FlatAwesome" [ text "LOADING ..." ]

        Error errorMsg ->
            viewWithHeader "Error | FlatAwesome" [ text errorMsg ]

        Done innerModel ->
            mapDocument InnerMsg (viewInner innerModel)
