port module Ports exposing
    ( localStore
    , log
    , logError
    , logInfo
    , logValue
    , logWarning
    )

import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)


port localStore : ( String, Maybe Value ) -> Cmd msg


port log : String -> Cmd msg


port logInfo : String -> Cmd msg


port logWarning : String -> Cmd msg


port logError : String -> Cmd msg


port logValue : ( String, Value ) -> Cmd msg
